class try24 {
    public static void main(String args[]){
//declare variables
        int month;
//ask for month number
        System.out.print ("Enter current month number: ");
//store it in variable
        month=keyboard.readInt();
//control statement
        if (month == 3 || month == 4 || month == 5)
            System.out.println ("Spring");
        else if (month == 6 || month == 7 || month == 8)
            System.out.println ("Summer");
        else if (month == 9 || month == 10 || month == 11)
            System.out.println ("Autumn");
        else if (month == 12 || month == 1 || month == 2)
            System.out.println ("Winter");
        else System.out.println ("Invalid month number");
    }
}

class try24bis {
    public static void main(String args[]){

        //declare variables
        int month;

        //ask for month number
        System.out.print ("Enter current month number: ");

        //store it in variable
        month = keyboard.readInt();
        switch (month) {
            case 3:
            case 4:
            case 5: System.out.println("Spring"); break;
            case 6:
            case 7:
            case 8: System.out.println("Summer"); break;
            case 9:
            case 10:
            case 11: System.out.println("Autumn"); break;
            case 12:
            case 1:
            case 2: System.out.println("Winter"); break;
            default: System.out.println("Invalid month");
        }
    }
}

class try26 {
    public static void main(String args[]){

        //declare variables
        int month;

        //ask for month number
        System.out.print ("Enter current month number: ");

        //store it in variable
        month = keyboard.readInt();
        switch (month) {
            case 1: System.out.println("January"); break;
            case 2: System.out.println("February"); break;
            case 3: System.out.println("March"); break;
            case 4: System.out.println("April"); break;
            case 5: System.out.println("May"); break;
            case 6: System.out.println("June"); break;
            case 7: System.out.println("July"); break;
            case 8: System.out.println("August"); break;
            case 9: System.out.println("September"); break;
            case 10: System.out.println("October"); break;
            case 11: System.out.println("November"); break;
            case 12: System.out.println("December"); break;
            default: System.out.println("Invalid month");
        }
    }
}

class try28 {
    public static void main(String args[]){
        int num1, num2, userChoice, answer;

//initialize variable answer
        answer=0;
        System.out.println (" \t Simple Calculator");
        System.out.print ("Enter a number: ");
        num1=keyboard.readInt();
        System.out.print ("Enter another number: ");
        num2=keyboard.readInt();
        System.out.println ("1. Addition");
        System.out.println ("2. Subtraction");
        System.out.println ("3. Multiplication");
        System.out.println ("4. Division ");
        System.out.print ("Make your choice: ");
        userChoice=keyboard.readInt();
        switch (userChoice) {
            case 1: answer=num1+num2; break;
            case 2: answer=num1-num2; break;
            case 3: answer=num1*num2; break;
            case 4: answer=num1/num2; break;
            default: System.out.println("Invalid option");
        }

        System.out.print ("Answer is " + answer);

    }
}

