import java.util.Scanner;

class prog12 {
    public static void main (String args[]) {

        Scanner s = new Scanner (System.in); //Creating object s from the class Scanner
        int number;
        int product;

        System.out.print("Please enter a number: ");
        number = s.nextInt();
        product = (number*2);

        System.out.println("Result is " + product);
    }
}

class prog13 {
    public static void main (String args[]) {

        Scanner s = new Scanner (System.in); //Creating object s from the class Scanner
        byte age;

        System.out.print("Please enter your age: ");
        age = s.nextByte();
        age+=5;

        System.out.println("By 5 years time you will be " + age + " years old.");
    }
}

class prog14 {
    public static void main (String args[]) {

        Scanner s = new Scanner (System.in); //Creating object s from the class Scanner
        int length;
        int volume;

        System.out.print("Please enter the length of the box in cm: ");
        length = s.nextInt();
        volume = (length*length*length);

        System.out.println("The volume of the box is " + volume + " cm3.");
    }
}

class prog15 {
    public static void main (String args[]) {

        Scanner s = new Scanner (System.in); //Creating object s from the class Scanner
        int dollars;
        double euros;

        System.out.print("Please enter amount of dollars to convert: ");
        dollars = s.nextInt();
        euros = (dollars*0.740466494);

        System.out.println(dollars + " dollars are " + euros + " euros.");
    }
}

class prog16 {
    public static void main (String args[]) {

        Scanner s = new Scanner (System.in); //Creating object s from the class Scanner
        double monday;
        double tuesday;
        double wednesday;
        double thursday;
        double friday;
        double saturday;
        double sunday;
        double avrgTemperature;


        System.out.print("Temperature on Monday: ");
        monday = s.nextDouble();
        System.out.print("Temperature on Tuesday: ");
        tuesday = s.nextDouble();
        System.out.print("Temperature on Wednesday: ");
        wednesday = s.nextDouble();
        System.out.print("Temperature on Thursday: ");
        thursday = s.nextDouble();
        System.out.print("Temperature on Friday: ");
        friday = s.nextDouble();
        System.out.print("Temperature on Saturday: ");
        saturday = s.nextDouble();
        System.out.print("Temperature on Sunday: ");
        sunday = s.nextDouble();

        avrgTemperature = ((monday+tuesday+wednesday+thursday+friday+saturday+sunday)/7);

        if (avrgTemperature<15) System.out.println("Average temperature this week is " + String.format("%.2f", avrgTemperature) + " ºC, brrrr... it's cold");
        else System.out.println("Average temperature this week is " + String.format("%.2f", avrgTemperature) + " ºC, wohooo!, it's not cold");
    }
}

class prog17 {
    public static void main (String args[]) {

        Scanner s = new Scanner (System.in); //Creating object s from the class Scanner
        int radius;
        double area;
        double circumference;

        System.out.print("Please enter the radius of the circumference in cm: ");
        radius = s.nextInt();
        area = (3.142*(radius*radius));
        circumference = (2*3.14*radius);

        System.out.println("Your circle has an area of " + area + " cm2 and a perimeter of " + circumference + " cm");
    }
}

class prog18 {
    public static void main (String args[]) {

        String nameItem1;
        byte quantityItem1;
        double priceItem1;

        String nameItem2;
        byte quantityItem2;
        double priceItem2;

        String nameItem3;
        byte quantityItem3;
        double priceItem3;

        double total;

        System.out.print("Please enter name for Item 1: ");
        nameItem1 = keyboard.readString();
        System.out.print("Please enter quantity for Item 1: ");
        quantityItem1 = keyboard.readByte();
        System.out.print("Please enter price for Item 1: ");
        priceItem1 = keyboard.readDouble()*quantityItem1;

        System.out.print("Please enter name for Item 2: ");
        nameItem2 = keyboard.readString();
        System.out.print("Please enter quantity for Item 2: ");
        quantityItem2 = keyboard.readByte();
        System.out.print("Please enter price for Item 2: ");
        priceItem2 = keyboard.readDouble()*quantityItem2;

        System.out.print("Please enter name for Item 3: ");
        nameItem3 = keyboard.readString();
        System.out.print("Please enter quantity for Item 3: ");
        quantityItem3 = keyboard.readByte();
        System.out.print("Please enter price for Item 3: ");
        priceItem3 = keyboard.readDouble()*quantityItem3;

        total = (priceItem1+priceItem2+priceItem3);
        total+=(total*0.18);

        System.out.println("\t Your Bill");
        System.out.println("Name \t Qty \t Price \t Total with VAT");
        System.out.println(nameItem1 + " \t " + quantityItem1 + " \t  \t" + priceItem1);
        System.out.println(nameItem2 + " \t " + quantityItem2 + " \t  \t" + priceItem2);
        System.out.println(nameItem3 + " \t " + quantityItem3 + " \t  \t" + priceItem3);
        System.out.println("\t \t \t \t \t \t \t" + total);
    }
}