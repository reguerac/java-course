class prog31 {
    public static void main(String args[]) {
        double temp;

        System.out.println("Please enter today's temperature in ºC: ");
        temp = keyboard.readDouble();

        if (temp > 20) System.out.println("WARM");
        else System.out.println("COLD");
    }
}

class prog32 {
    public static void main(String args[]) {
        byte mark;

        System.out.println("Please enter your mark: ");
        mark = keyboard.readByte();
/*
        while (mark<=1 || mark>=7){
            System.out.println("Please enter a numeric mark from 1 to 7: ");
            mark = keyboard.readByte();
        }
*/
        switch (mark) {
            case 1:
                System.out.println("Paper A: Excellent! \n Now you know everything! \n You deserve a holiday!");
                break;
            case 2:
                System.out.println("Paper A: Very well done! \n This shows you studied well!");
                break;
            case 3:
                System.out.println("Paper A: Good job!");
                break;
            case 4:
                System.out.println("Paper A: Not bad! \n Paper B: Well done!");
                break;
            case 5:
                System.out.println("Paper A: Study Harder \n Paper B: Good job!");
                break;
            case 6:
                System.out.println("Paper B: Not bad!");
                break;
            case 7:
                System.out.println("Paper B: Study Harder!");
                break;
            default:
                System.out.println("Fail");
        }
    }
}