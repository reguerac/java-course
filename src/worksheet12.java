

class Prog37{
    public static void main (String args[]){
        int[] tempDay = new int[7];
        String[] day = new String[7];
        int i;
        tempDay[0] = 15; day[0]="Monday";
        tempDay[1] = 11; day[1]="Tuesday";
        tempDay[2] = 14; day[2]="Wednesday";
        tempDay[3] = 16; day[3]="Thursday";
        tempDay[4] = 12; day[4]="Friday";
        tempDay[5] = 17; day[5]="Saturday";
        tempDay[6] = 16; day[6]="Sunday";

        //Temp on Monday
        System.out.println ("Monday: " + tempDay[0]);

        //Rest of the week
        for (i=1 ; i<=6 ; i++) {
            System.out.println (day[i] + ": " + tempDay[i]);
        }
    }
}

class Prog37bis{
    public static void main (String args[]){

        String[] day = new String[7];
        int i;
        int[] tempDay = {15,11,14,16,12,17,16};

        day[0]="Monday";
        day[1]="Tuesday";
        day[2]="Wednesday";
        day[3]="Thursday";
        day[4]="Friday";
        day[5]="Saturday";
        day[6]="Sunday";

        //Temp on Monday
        System.out.println ("Monday: " + tempDay[0]);

        //Rest of the week
        for (i=1 ; i<=6 ; i++) {
            System.out.println (day[i] + ": " + tempDay[i]);
        }
    }
}

class Prog38 {
    public static void main (String args[]){
        int i; //used for loop
        String[] countries = new String[7]; //declare array countries
        String[] capitals = new String[7]; //declare array capitals

//fill array countries with information
        countries[0] = "Malta";
        countries[1] = "England";
        countries[2] = "Italy";
        countries[3] = "Germany";
        countries[4] = "France";
        countries[5] = "Spain";
        countries[6] = "Greece";

//ask user to enter data in array capitals
        for (i=0;i<7;i++){
            System.out.print ("What is the capital city of " + countries[i] + ": ");
            capitals[i]=keyboard.readString();
        }
//display all information
        for (i=0;i<7;i++){
            System.out.println (countries[i] + " capital city : " + capitals[i]);
        }
    }
}

class Prog38bis {
    public static void main (String args[]){
        int i; //used for loop
        String[] countries = new String[7]; //declare array countries
        String[] capitals = new String[7]; //declare array capitals
        String[] correctCap = {"Valletta","London","Rome","Berlin","Paris","Madrid","Athens"}; //declare array for correct answers that will be displayed later on

//fill array countries with information
        countries[0] = "Malta";
        countries[1] = "England";
        countries[2] = "Italy";
        countries[3] = "Germany";
        countries[4] = "France";
        countries[5] = "Spain";
        countries[6] = "Greece";

//ask user to enter data in array capitals
        for (i=0;i<7;i++){
            System.out.print ("What is the capital city of " + countries[i] + ": ");
            capitals[i]=keyboard.readString();
        }
//display all information
        for (i=0;i<7;i++){
            System.out.println ("You said that " + countries[i] + "'s capital city is " + capitals[i]);
                    if(capitals[i].equals(correctCap[i])){ //If else to check correct answers
                        System.out.println("Well done, your answer is correct");
                    } else {
                        System.out.println("Incorrect, the correct answer is " + correctCap[i]);
                    }
        }
    }
}