class prog09{
    public static void main (String args[]){

        // Declaring variables
        int num1, num2, ansAdd, ansSub, ansMult, ansDiv;

        //Initialising variables
        num1 = 50;
        num2 = 5;

        //Operating
        ansAdd=num1+num2;
        ansSub=num1-num2;
        ansMult=num1*num2;
        ansDiv=num1/num2;

        //Outputting answers

        System.out.println(ansAdd);
        System.out.println(ansSub);
        System.out.println(ansMult);
        System.out.println(ansDiv);
    }
}

class prog10{
    public static void main (String args[]){

        //Declaring variables
        float rainJan, rainFeb, rainMar, totalRain, avrgRain;

        //Initialising variables

        rainJan = 94.5f;
        rainFeb = 65.9f;
        rainMar = 38.1f;

        //Calculating
        totalRain = (rainJan+rainFeb+rainMar);
        avrgRain = totalRain/3;

        //Outputting

        System.out.println("The total rainfall in the first three" +
                " months of the year is " + totalRain + "mm");
        System.out.println("The average rainfall in the first three" +
                " months of the year is " + avrgRain + "mm");
    }
}

class prog11{
    public static void main (String args[]){

        // Declaring variables
        int num1, num2, temp;

        //Initialising variables
        num1 = 15;
        num2 = 5;

        //Outputting variables before swap
        System.out.println("Before Swap");
        System.out.println("Num 1 is " + num1);
        System.out.println("Num 2 is " + num2);

        //Operating
        temp = num1;
        num1 = num2;
        num2 = temp;

        //Outputting variables after swap
        System.out.println("\n");
        System.out.println("After Swap");
        System.out.println("Num 1 is " + num1);
        System.out.println("Num 2 is " + num2);
    }
}
