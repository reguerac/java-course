class prog19 {
    public static void main(String args[]) {

        //Declare variable
        int age;

        //Init variable
        System.out.print("Please enter your age: ");
        age = keyboard.readInt();

        //Output
        if (age >= 18) System.out.println("You can drive");
        else System.out.println("Sorry you cannot drive");
    }
}

class prog20 {
    public static void main(String args[]) {

        //Declare variables
        double price1;
        double price2;
        double price3;
        double totalPrice;
        double discountedPrice;

        //Initialising variables
        System.out.print("Please enter price for item 1: ");
        price1 = keyboard.readDouble();
        System.out.print("Please enter price for item 2: ");
        price2 = keyboard.readDouble();
        System.out.print("Please enter price for item 3: ");
        price3 = keyboard.readDouble();

        totalPrice = (price1 + price2 + price3);

        if (totalPrice >= 40) {
            discountedPrice = totalPrice - (totalPrice * 0.10);
            System.out.println("Total price is " + totalPrice + " euros, but you get a 10% discount and your final price is " + discountedPrice);
        } else System.out.println("Total price is " + totalPrice);

    }
}

class prog21 {
    public static void main(String args[]) {

        //Declare variables
        byte month;

        //Initialising variables
        System.out.println("Please enter month number: ");
        month = keyboard.readByte();

        if (month >= 1 && month <= 12) System.out.println("Valid Month");
        else System.out.println("Invalid Month");
    }
}

class prog22 {
    public static void main(String args[]) {

//declare variables
        byte month;

//ask for month number
        System.out.print("Enter current month number: ");

//store it in variable
        month = keyboard.readByte();
        switch (month) {
            case 1:
                System.out.print("January");
                break;
            case 2:
                System.out.print("February");
                break;
            case 3:
                System.out.print("March");
                break;
            case 4:
                System.out.print("April");
                break;
            case 5:
                System.out.print("May");
                break;
            case 6:
                System.out.print("June");
                break;
            case 7:
                System.out.print("July");
                break;
            case 8:
                System.out.print("August");
                break;
            case 9:
                System.out.print("September");
                break;
            case 10:
                System.out.print("October");
                break;
            case 11:
                System.out.print("November");
                break;
            case 12:
                System.out.print("December");
                break;
            default:
                System.out.print("Invalid month");
        }
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
            System.out.print(" has 31 days");
        else if (month == 4 || month == 6 || month == 9 || month == 11) System.out.println(" has 30 days");
        else if (month == 2) System.out.println(" has 28 days");
    }
}

class prog23 {
    public static void main(String args[]) {

//declare variables
        int userChoice;
        double usDollars, euro;

//show menu
        System.out.println("Calculator");
        System.out.println();
        System.out.println("1. U.S. Dollars --> Euros");
        System.out.println("2. Euros --> U.S. Dollars");
        System.out.println();
        System.out.println("Choose which conversion you would like.");
        userChoice = keyboard.readInt(); //read user's choice
        switch (userChoice) {
            case 1: {
                System.out.println();
                System.out.print("Enter an amount in U.S. Dollars: ");
                usDollars = keyboard.readFloat();
                euro = usDollars * 0.74;
                System.out.println(euro + " Euros");
                break;
            }
            case 2: {
                System.out.println();
                System.out.print("Enter an amount in Euros: ");
                euro = keyboard.readFloat();
                usDollars = euro * 1.24;
                System.out.println(usDollars + " U.S. Dollars");
                break;
            }
            default:
                System.out.println("Invalid choice");
        }
    }
}

class prog24 {
    public static void main(String args[]){
//declare variables
        int num1, num2,userChoice,correctAns,userAnswer;
//initialize variables
        correctAns=0;
        userAnswer=0;
//generate random numbers (between 1 and 10)
        num1=(int)(Math.random()*(10));
        num2=(int)(Math.random()*(10));
//show menu
        System.out.println ("Mathematical Quiz");
        System.out.println ();
        System.out.println ("1. Addition");
        System.out.println ("2. Subtraction");
        System.out.println ("3. Multiplication");
        System.out.println ();
        System.out.println ("On which operation would you like to be quizzed? ");
                userChoice=keyboard.readInt(); //read user's choice
        switch (userChoice){
            case 1 :{
                correctAns=num1+num2;
                System.out.println ();
                System.out.print (num1 + " + " + num2 + " = ");
                userAnswer=keyboard.readInt();
                break;
            }
            case 2 :{
                correctAns=num1-num2;
                System.out.println ();
                System.out.print (num1 + " - " + num2 + " = ");
                userAnswer=keyboard.readInt();
                break;
            }
            case 3 :{
                correctAns=num1*num2;
                System.out.println ();
                System.out.print(num1 + " * " + num2 + " = ");
                userAnswer=keyboard.readInt();
                break;
            }
            default: System.out.println("Invalid choice");
        }
        if (userAnswer==correctAns) System.out.println ("Correct");
        else System.out.println ("Incorrect, the correct answer is " + correctAns);
    }
}

