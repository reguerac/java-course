class prog25 {
    public static void main(String args[]) {
        int i;

        for (i = 1; i <= 4; i++) {
            System.out.print(" * ");
        }
    }
}

class prog25Bis {
    public static void main(String args[]) {
        int i, numberChars;

        System.out.println("Please enter the number of times you wish the *: ");
        numberChars = keyboard.readInt();

        for (i = 1; i <= numberChars; i++) {
            System.out.print(" * ");
        }
    }
}

class prog26 {
    public static void main(String args[]) {
        int i;

        for (i = 1; i <= 8; i++) {
            System.out.println("Computing ");
        }
    }
}

class prog27 {
    public static void main(String args[]) {
        int i;
        for (i = 1; i <= 5; i++) {
            System.out.println(i + " * ");
        }
    }
}

class prog28 {
    public static void main(String args[]) {
        int i, num, total;
        total = 0; //initialize variable total
        for (i = 1; i <= 5; i++) {
            System.out.print("Enter number " + i + ": ");
            num = keyboard.readInt();
            total = total + num;
        }
        System.out.println("Total is " + total);
    }
}

class prog29 {
    public static void main(String args[]) {
        int i, students;
        double mark, sumMark, avrgMark;

        System.out.print("Please enter the number of students: ");
        students = keyboard.readInt();
        sumMark = 0;
        for (i = 1; i <= students; i++) {
            System.out.print("Enter mark for student " + i + ": ");
            mark = keyboard.readDouble();
            sumMark = (sumMark + mark);
        }
        avrgMark= sumMark/students;
        System.out.println("Average mark is " + avrgMark);
    }
}

class prog30 {
    public static void main(String args[]){
        //declare variables
        int userChoice;
        //show menu
        System.out.println ();
        System.out.println ("Mathematical Quiz");
        System.out.println ();
        System.out.println ("1. Addition");
        System.out.println ("2. Subtraction");
        System.out.println ("3. Multiplication");
        System.out.println ("4. Exit");
        System.out.println ();
        System.out.print ("On which operation would you like to be quizzed?");
        userChoice=keyboard.readInt(); //read user's choice
        while (userChoice > 4 || userChoice <1) {
            System.out.print ("Invalid choice. On which operation would you like to be quizzed? ");
                    userChoice=keyboard.readInt(); //read user's choice
        }
    }
}

class prog30Bis {
    public static void main(String args[]){

        //declare variables
        int userChoice;
        do {
            //show menu
            System.out.println ();
            System.out.println ("Mathematical Quiz");
            System.out.println ();
            System.out.println ("1. Addition");
            System.out.println ("2. Subtraction");
            System.out.println ("3. Multiplication");
            System.out.println ("4. Exit");
            System.out.println ();
            System.out.print ("On which operation would you like to be quizzed?");
                    userChoice=keyboard.readInt(); //read user's choice
            while (userChoice > 4 || userChoice <1) {
                System.out.print ("Invalid choice. On which operation would you like to be quizzed? ");
                        userChoice=keyboard.readInt(); //read user's choice
            }
        } while (userChoice!=4);
    }
}

class try27 {
    public static void main(String args[]){
        int num;
        num = 0;

        do {
            System.out.print("Please enter a number between 1 and 10: ");
            num=keyboard.readInt();
        } while (num<1 || num>10);
        System.out.print("Thank you for your input");
    }
}