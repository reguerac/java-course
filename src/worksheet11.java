
class prog34 {
    public static void main(String args[]) {
            //declare variables
        byte count;

            //generate a random number between 1 and 10
        for(count = 1; count <= 5; count++) {
            System.out.println("Random number " + count + " is: " + ((int) (Math.random() * (45))+1));
        }
    }
}

class prog35{
    public static void main (String args[]){
//declare variables
        int num, pow;
        double answer;
        System.out.print ("Enter a number: "); //ask user to input a number
        num=keyboard.readInt(); //user inputs a number in variable num

        System.out.print ("Enter a power: "); //ask user to input a power
        pow=keyboard.readInt(); //user inputs a power in variable pow

        answer=Math.pow(num,pow); //answer = num to the power of pow
        System.out.println (num + " to the power of " + pow + " is " + answer); //show answer
    }
}

class Prog36{
    public static void main (String args[]){
//declare variables
        int num;
        double answer;
        System.out.print ("Enter a number: "); //ask user to input a number
        num=keyboard.readInt(); //user inputs a number in variable num
        answer=Math.sqrt(num); //answer = sqrt of num
        System.out.println ("The square root of " + num + " is " + answer); //show answer
    }
}